
import './App.css';
import React, { useState, useContext } from 'react'
import MainMenu from './component/MainMenu';
import EndScreen from './component/EndScreen';
import Quiz from './component/Quiz';
// import RadioGroup from '@mui/material';
// import FormControlLabel from '@mui/material';
// import FormControl from '@mui/material';
// import { Typography } from '@mui/material';
import Radio from '@mui/material/Radio';
import { QuizContext } from './Helpers/Contexts'

function App() {

  const [gameState, setGameState] = useState("menu");
  const [score, setScore] = useState(0)

  return (
    <div class="row">
     <div class="col-xs-6  col-md-4">
        <div className="App">
          {" "}
          <h2>Quiz</h2>
          <QuizContext.Provider value={{ gameState, setGameState, score, setScore }}>
            {gameState === "menu" && <MainMenu />}
            {gameState === "quiz" && <Quiz />}
            {gameState === "endScreen" && <EndScreen />}
          </QuizContext.Provider>


        </div>
      </div>
    </div>
  );
}

export default App;
