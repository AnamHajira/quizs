import React,{useState, useContext, useRef, useEffect} from 'react'
import {Questions} from '../Helpers/QuestionBank';
import{QuizContext} from '../Helpers/Contexts';
import Radio from '@mui/material/Radio';

function Quiz() {
 
   setTimeout(() => {
      nextQuestion()
      
      
       },5000);

  

      
       const finishQuiz = () =>{
         if(Questions[currentQuestion].answer == optionChosen){
             setScore(score+1);
          }
          setGameState("endScreen");
 
     };
  
   
   // const timout =setTimeout(() => {
   //    nextQuestion()
   // }, 5000);
    const { score, setScore,setGameState } =useContext(QuizContext);
    const [currentQuestion, setCurrentQuestion] = useState(0); 
    const [optionChosen, setOptionChosen] = useState("")
    const [buttonDisabled, setButtonDisabled] = useState(true)

//     let index = 0;
// let scores = 0;
// const nextQues = () => {
//   if (index > Questions.length - 1) {
//     gameOver();
//     return;
//   } else {
//     let question = document.getElementById("question");
//     question.innerText = Questions[index].ques;
//     for (let i = 0; i < 4; i++) {
//       let option = document.getElementById("option-" + (i + 1));
//       option.innerText = Questions[index].optionChosen[i];
//       option.className = "blue-bg";
//       if (Questions[index].correctIndex === i) {
//         option.onclick = (e) => {
//           e.target.className = "right";
//           scores++;
//           e.target.onclick = null;
//         };
//       } else {
//         option.onclick = (e) => {
//           e.target.classList.remove("blue-bg");
//         };
//       }
//     }
//     index++;
//     setTimeout(nextQues, 10000);
//   }
// };
// nextQues();
// function gameOver() {
//   document.getElementById("gameOver").classList.remove("hidden");
//   document.querySelector(".timer").style.animation = "none";
//   document.querySelector(".timer").style["background-color"] = "gray";
//   document.getElementById("final-scores").innerText = scores;
// }


   //  const timer = useRef(null);
   //  const progressBar = useRef(null);

   //  function gotoNextQuestion(){
   //    if(timer.current){
   //       clearTimeout(timer.current);
   //    }
   //    // flushSync(()=>{
   //    // setOptionChosen(null);
   //    // });
      
   //  }
   
   //  useEffect(()=>{
   //    progressBar.current.classList.remove("active");
   //    setTimeout(()=>{
   //       progressBar.current.classList.add("active");
   //    },0);
   //    timer.current = setTimeout(gotoNextQuestion,5*1000);
   //    return gotoNextQuestion;

   //  },);

    const nextQuestion = ()=>{
     if(Questions[currentQuestion].answer == optionChosen){
        setScore(score+1);
     }
    //  alert(score)
     setCurrentQuestion(currentQuestion+1);

    };
   

    const previousQuestion = ()=>{
      if(Questions[currentQuestion].answer == optionChosen){
         setScore(score-1);
      }
     //  alert(score)
      setCurrentQuestion(currentQuestion-1);
 
     };
 

   //  const finishQuiz = () =>{
   //      if(Questions[currentQuestion].answer == optionChosen){
   //          setScore(score+1);
   //       }
   //       setGameState("endScreen");

   //  };

    const submitQuestion = () =>{
      if(Questions[currentQuestion].answer === optionChosen){
          setScore(score+1);
       }
       setGameState("endScreen");

  };
//   const uncheckFunction =() =>{
//    let allRadioButtons = document.querySelectorAll('radioButtons');
//    allRadioButtons.forEach(()=> this.checked = false)
//   }

// const uncheckFunction = () =>{
//    var radio =document.getElementById('prompt.optionA');
//    radio.checked = false;
// }

  const radioClick = () =>{
   setButtonDisabled(false)
  }
  return (
  
   
    <div className="Quiz">
    <h2>{Questions[currentQuestion].prompt}</h2>
    
    <div className='options'>
    
        <button onClick={()=> setOptionChosen("A")}>
       <label>
        <input type='radio' name='radioSelect'  onChange={radioClick} className='radioButton' id={prompt.optionA}
         
        />
        </label>
   
       {Questions[currentQuestion].optionA}{" "}</button>
        
        <button onClick={()=> setOptionChosen("B")}>
        <label>
        <input type='radio' name='radioSelect'  onChange={radioClick} className='radioButton' id={prompt.optionB}
         
         />
         </label>
        {Questions[currentQuestion].optionB}{" "}</button>
         
         <button onClick={()=> setOptionChosen("C")}>
         <label>
         <input type='radio' name='radioSelect'  onChange={radioClick} className='radioButton' id={prompt.optionC} />
         </label>
        {Questions[currentQuestion].optionC}{" "}</button>

        <button onClick={()=> setOptionChosen("D")}>
        <label>
        <input type='radio' name='radioSelect' onChange={radioClick} className='radioButton' id={prompt.optionD}
        
        
        />
        </label>
        {Questions[currentQuestion].optionD}{" "}</button>
        
    </div>
     {currentQuestion === Questions.length -1 ? (
       <>
      
       <button onClick={previousQuestion}> Previous</button>
        <button onClick={ finishQuiz}>Finish Quiz</button>
       
        <button onClick={ submitQuestion}>Submit</button>

        </>
     ):(
        <><button  onClick={nextQuestion}> Next </button>
        <button onClick={previousQuestion}> Previous</button>
        {/* <button onClick={uncheckFunction}>Unchecked</button> */}
        <button onClick={ submitQuestion}>Submit</button></>
     )
     }
    
    </div>

  )
}

export default Quiz
